import time


class Counties():
    counties_list = ["Alba", "Arad", "Argeș", "Bacău", "Bihor", "Bistriţa-Năsăud", "Botoşani", "Braşov", "Brăila", "Bucureşti", "Buzău", "Caraş-Severin",
                     "Călăraşi", "Cluj", "Constanţa", "Covasna", "Dâmboviţa", "Dolj", "Galaţi", "Giurgiu", "Gorj", "Harghita", "Hunedoara", "Ialomiţa",
                     "Iaşi", "Ilfov", "Maramureş", "Mehedinţi", "Mureş", "Neamţ", "Olt", "Prahova", "Satu Mare", "Sălaj", "Sibiu", "Suceava", "Teleorman",
                     "Timiş", "Tulcea", "Vâlcea", "Vaslui", "Vrancea"]

    def __init__(self, county_name):
        self.county_name = county_name
        self.createdTime = time.strftime('%Y-%m-%d %H:%M:%S')


class Localities(Counties):

    def __init__(self, county_name, locality_name):
        super().__init__(county_name)
        self.locality_name = locality_name
