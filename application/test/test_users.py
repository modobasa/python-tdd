import unittest
import sys
import os

try:
    sys.path.insert(
    0, 'C:\\Users\\modobasa\\Documents\\Python\\StockReport TDD\\application\\users\\')
    from models import Users
except:
    print("Error loading module!")

# result = Users("admin", "Marius", "Odobasa",
#                    "modobasa@pentalog.com", "master", True, True)
# result.save()


class TestUsers(unittest.TestCase):

    def test_if_is_any_instance(self):     
        result = Users.load(self)        
        self.assertIsInstance(result,Users)
    
    def test_if_is_any_user_defined_as_admin(self):               
        result = Users.load(self)
        self.assertEqual(result.username, "admin")

    def test_if_user_is_active(self):
        result = Users.load(self)
        result.is_active = False
        self.assertEqual(result.is_active, False)        

    def test_number_of_instances(self):
        result = Users.load(self)               
        self.assertEqual(result.id, 1)

if __name__ == '__main__':
    unittest.main()
