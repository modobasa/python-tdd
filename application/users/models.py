from datetime import date
import time
import pickle
import weakref
import os
from itertools import count


class Users():
    _counter = 0
    
    def __init__(self, username, firstname, lastname, email, user_rights, is_admin, is_active):
        Users._counter += 1
        self.id = Users._counter
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.user_rights = user_rights 
        self.date_joined = time.strftime('%Y-%m-%d %H:%M:%S')
        self.last_login = date.today()
        self.is_admin = is_admin
        self.is_active = is_active       
       
        try:
            self.load()
        except:
            self.save()

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(
                key=key, value=self.__dict__[key]))

        return ', '.join(sb)

    def __repr__(self):
        return self.__str__()

    def save(self):
        with open("database.db", "wb") as save_instances:
            pickle.dump(self, save_instances)
    
    def load(self):
        try:
            with open("database.db", "rb") as load_instances:
                users_instances = pickle.load(load_instances)
        except:
            print("No saved data found!")
        return users_instances

    @property
    def fullname(self):
        return '{} {}'.format(self.firstname, self.lastname)


class Masters(Users):

    def __init__(self, username, firstname, lastname, email, user_rights, is_admin, is_active):
        super().__init__(username, firstname, lastname,
                         email, user_rights, is_admin, is_active)

        self.date_joined = time.strftime('%Y-%m-%d %H:%M:%S')


class Managers(Users):

    def __init__(self, username, firstname, lastname, email, user_rights, is_admin, is_active):
        super().__init__(username, firstname, lastname,
                         email, user_rights, is_admin, is_active)

        self.date_joined = time.strftime('%Y-%m-%d %H:%M:%S')


class Merchandisers(Users):

    def __init__(self, username, firstname, lastname, email, user_rights, is_admin, is_active):
        super().__init__(username, firstname, lastname,
                         email, user_rights, is_admin, is_active)

        self.date_joined = time.strftime('%Y-%m-%d %H:%M:%S')


# user = Users("admin", "Marius2", "Odobasa",
#                 "modobasa@pentalog.com", "master", True, True)


# print("==== [Class Instances] ====", end="\n")
# for instance in Users.instances:
#     print(instance)
